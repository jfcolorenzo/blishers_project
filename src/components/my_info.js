import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import LeftMenu from './left_menu';
import LogoTop from './logo_top';
import ProfileTopMenu from './profile_top_menu'

require('./styles/my_info.css');

class MyInfo extends Component {

  render(){
    return(
      <div className="container  container_my_info">
                <div className="row">
                  <div className="col-2">
                    <LogoTop />
                  </div>

                  <div className="col-10 ">
                    <ProfileTopMenu />
                  </div>

                </div>

                <div className="row body_content">

                  <div className="col-2  center_horizontally center_vertically">
                    <LeftMenu />
                  </div>

                  <div className="col-9 ">

                    <div className="row">
                      <div className="col  center_horizontally title_my_info">
                        <p>I want to sell / buy rights and must complete the details</p>
                      </div>
                    </div>

                    <div className="row">

                        <div className="col center_horizontally ">
                            <div className="info_input_group">
                              <input type="text" className="input_tbox_light" placeholder="First Name" />
                              <div className="edit_button noselect">Edit</div>
                            </div>
                            <div className="info_input_group">
                              <input type="text" className="input_tbox_light" placeholder="Last Name" />
                              <div className="edit_button noselect">Edit</div>
                            </div>
                            <div className="info_input_group">
                              <input type="text" className="input_tbox_light" placeholder="Enter your Email" />
                              <div className="edit_button noselect">Edit</div>
                            </div>
                            <div className="info_input_group">
                              <input type="text" className="input_tbox_light" placeholder="Enter your Password" />
                              <div className="edit_button noselect">Edit</div>
                            </div>
                            <div className="info_input_group">
                              <input type="text" className="input_tbox_light" placeholder="Company Name" />
                              <div className="edit_button noselect">Edit</div>
                            </div>
                            <div className="info_input_group">
                              <input type="text" className="input_tbox_light" placeholder="Website" />
                              <div className="edit_button noselect">Edit</div>
                            </div>
                            <div className="info_input_group">
                              <input type="text" className="input_tbox_light" placeholder="Phone" />
                              <div className="edit_button noselect">Edit</div>
                            </div>
                        </div>

                        <div className="col center_horizontally ">
                          <div className="info_input_group">
                            <input type="text" className="input_tbox_light" placeholder="Company Contact" />
                            <div className="edit_button noselect">Edit</div>
                          </div>
                          <div className="info_input_group">
                            <textarea className="input_tbox_light_large" placeholder="Description" rows="5"/>
                            <div>
                              <div className="edit_button noselect">Edit</div>
                              <div className="fake_edit_button_light"></div>
                            </div>
                          </div>
                          <div className="info_input_group">
                            <textarea  className="input_tbox_light_large" placeholder="Address" rows="5"/>
                              <div>
                                <div className="edit_button noselect">Edit</div>
                                <div className="fake_edit_button_light"></div>
                              </div>
                          </div>
                          <div className="info_input_group">
                            <input type="text" className="input_tbox_light" placeholder="Time Zone" />
                            <div className="edit_button noselect">Edit</div>
                          </div>
                          <div className="info_input_group">
                            <input type="text" className="input_tbox_light" placeholder="Vat ID" />
                            <div className="edit_button noselect">Edit</div>
                          </div>
                        </div>

                    </div>

                    <div className="row">
                      <div className="col  center_horizontally">
                        <Link className="link_white" to="/report"><div className="menu_bar_dark center_vertically noselect" style={{marginTop:40}}>Sign Up</div></Link>
                      </div>
                    </div>

                </div>

                <div className="col-1 ">
                </div>

              </div>
      </div>
    );
  }
}

export default MyInfo;
