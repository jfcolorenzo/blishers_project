import React, { Component } from 'react';
import LogoTop from './logo_top';
require('./styles/home_top_bar.css');

class HomeTopBar extends Component {

  render(){
    return(
      <div className="">
        <div className="row">
          <div className="col-2">
            <LogoTop />
          </div>
          <div className="col-10">
              <div className="home_menu_top_bar">
                <ul>
                  <li className="home_menu_top_bar menu_item">About Us</li>
                  <li className="home_menu_top_bar menu_item">Contact Us</li>
                  <li className="home_menu_top_bar menu_item">Language</li>
                </ul>
              </div>
          </div>
        </div>
      </div>
    );
  }
}

export default HomeTopBar;
