import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Auth from '../Auth/Auth.js';



import HomeTopBar from './home_top_bar';

require('./styles/home.css');

class Home extends Component {

  render(){

    const auth = new Auth();


    const WORLD_TEXT = "A marketplace to buy and sell rights worldwide, bespoke software allows users to list, search for, enquire on, negotiate, complete deals, pay and charge the operation.";
    const INFO_TEXT = "A large international subscriber base of rights buyers can discover the hottest new clients and suppliers in a format that works alongside of the other business systems.";
    const BAG_TEXT = "Sellers can create their own online marketplace to sell rights globally, advertise and market their contents via a newsletter.";
    const CART_TEXT = "Buyers can browse, make targeted searches and for whole contents rights and permissions, 24/7, 365 days a year, all from the desktop, tablet and smartphone.";

    return(
      <div>
        <div className="container container_home ">
          <div className="row">
            <div className="col-12">
              <HomeTopBar />
            </div>
          </div>
          <div className="row body_content">
            <div className="col-3">
            </div>
            <div className="col-6 ">
              <div className="menu_home">
                <div>Don't have an account?</div>
                <div className="menu_bar_dark">Sign Up</div>
                <div className="center_vertically light_font">Or</div>
                <input type="text" className="menu_bar_tbox_light" placeholder="Email" />
                <input type="password" className="menu_bar_tbox_light" placeholder="Password" />
                <div className="menu_bar_dark" style={{margin: "20px 0 0 0"}}><Link className="link_white" to="/wannado">Log In</Link></div>
              </div>
            </div>
            <div className="col-3">
            </div>
          </div>
          <div className="row bottom_info_container">
            <div className="col">
              <div className="bottom_info">
                <img src={require('./images/1A.jpg')} alt="Martketplace"/>
                <p> {WORLD_TEXT} </p>
              </div>
            </div>
            <div className="col">
              <div className="bottom_info gray_light">
                <img src={require('./images/2A.jpg')} alt="Info"/>
                <p>{INFO_TEXT} </p>
              </div>
            </div>
            <div className="col">
              <div className="bottom_info gray_light">
                <img src={require('./images/3A.jpg')} alt="Sell contents"/>
                <p>{BAG_TEXT}</p>
              </div>
            </div>
            <div className="col">
              <div className="bottom_info">
                <img src={require('./images/4A.jpg')} alt="Buy contents"/>
                <p>{CART_TEXT}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      );
  }

}


export default Home;
