import React, { Component } from 'react';
require('./styles/profile_top_menu.css');

class ProfileTopMenu extends Component {

  render(){
    /*TO-DO*/
    const KIND_USER= 'SELLER'; /* get from current logued user */
    const ACTIVE_ITEM= 'MY INFORMATION'; /* get by props and change className */

    return(
      <div className="profile_menu">
        <ul className="profile_menu">
          <li className="profile_menu_inactive_item">{KIND_USER}</li>
          <li><div className="profile_menu_vertical_line"></div></li>
          <li className="profile_menu_inactive_item">BOOKS</li>
            <li><div className="profile_menu_vertical_line"></div></li>
            <li className="profile_menu_active_item">{ACTIVE_ITEM}</li>
        </ul>
      </div>
      );
    }
}

export default ProfileTopMenu;
