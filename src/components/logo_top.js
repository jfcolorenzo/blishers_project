import React, { Component } from 'react';
import { Link } from 'react-router-dom';

require('./styles/logo_top.css');

class LogoTop extends Component {

  render(){
    return(
      <div>
        <Link className="link" to="/"><img src={require('./images/logo.jpg')} alt="Blishers"/></Link>      
      </div>
    );
  }

}


export default LogoTop;
