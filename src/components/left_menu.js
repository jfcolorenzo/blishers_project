import React, { Component } from 'react';
import { Link } from 'react-router-dom';

require('./styles/left_menu.css');



class LeftMenu extends Component {

  render(){
    return(
      <div className="list_left_menu">
        <ul className="list_left_menu">
          <li><Link className="list_link" to="/my_info">My Information</Link></li>
          <li>Publication Fields</li>
          <li>Password and Security</li>
          <li>Billing Methods</li>
          <li>Notifications</li>
          <li>Permissions and Conditions</li>
        </ul>
      </div>
      );
    }
}

export default LeftMenu;
