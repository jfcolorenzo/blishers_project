import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import HomeTopBar from './home_top_bar';
require('./styles/wannado.css');

class Wannado extends Component{
  render(){

    return(
        <div className="container container_wannado">
          <div className="row">
            <div className="col-12">
              <HomeTopBar />
            </div>
          </div>
          <div className="row body_content" >
            <div className="col-12">
              <p>Let's get started</p>
              <p>First tell us what do you want to do</p>
            </div>
          </div>
          <div className="row body_content">
            <div className="col-5 center_div_img">
              <Link className="role_img" to="/my_info"><img  src={require('./images/buyer.jpg')} alt="Seller"/></Link>
            </div>
            <div className="col-2 center_div_img">
              <div className="vertical_line"></div>
            </div>
            <div className="col-5 center_div_img">
              <Link className="role_img" to=""><img  src={require('./images/seller.jpg')} alt="Buyer"/></Link>
            </div>
          </div>

          <div className="row">
            <div className="col-5">
              I am looking for contents
            </div>
            <div className="col-2">
              Or
            </div>
            <div className="col-5">
              I want to sell rights
            </div>
          </div>

          <div className="row">
            <div className="col-5">

            </div>
            <div className="col-2 center_div_img">
              <div className="vertical_line"></div>
            </div>
            <div className="col-5">

            </div>
          </div>

        </div>
    );
  }
}

export default Wannado;
