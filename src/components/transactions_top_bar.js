import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import LogoTop from './logo_top'
require('./styles/transactions_top_bar.css');

class TransactionsTopBar extends Component {


 transaction_menu(section){
   var fields = [
      {text: 'MARKETPLACE'},
      {text: 'FOLLOWERS'},
      {text: 'REPORTS'},
      {text: 'PUBLICATION LIST'}
    ];

   return(
       fields.map(function(field , index) {
           if(index == section)
             return <li className="transaction_menu transaction_menu_item_active">{field.text}</li>;
           else
             return <li className="transaction_menu transaction_menu_item">{field.text}</li>;
        })
   );
 }

  render(){
    /*TO-DO*/
    const KIND_USER= 'SELLER'; /* get from current logued user */
    const ACTIVE_ITEM= 'MY INFORMATION'; /* get by props and change className */

    return(
      <div>
        <div className="row">
            <div className="col-6">
              <LogoTop />
                <div className="navigation_menu">
                  <ul className="navigation_menu">
                    <li className="navigation_menu_inactive_item">{KIND_USER}</li>
                    <li><div className="navigation_menu_vertical_line"></div></li>
                    <li className="navigation_menu_inactive_item">BOOKS</li>
                      <li><div className="navigation_menu_vertical_line"></div></li>
                      <li className="navigation_menu_active_item">{ACTIVE_ITEM}</li>
                  </ul>
                </div>
            </div>
            <div className="col-6">
              <div className="row">
                <div className="col-4">
                </div>
                <div className="col-5">
                  <div className="row">
                    <Link className="link_black" to="/my_info" params="userId">
                      <img className="" src={require('./images/linea_editorial.jpg')} alt=""/>
                    </Link>
                      <span className="actions_menu_nameof_actor">Línea Editorial</span>

                  </div>
                  <div className="row actions_menu">
                    <div className="col-3 actions_menu_icon">
                      <img className="" src={require('./images/message.jpg')} alt="Message"/>
                      <span>Message</span>
                    </div>
                    <div className="col-3 actions_menu_icon">
                      <img className="" src={require('./images/message.jpg')} alt="Chat"/>
                      <span>Chat</span>
                    </div>
                    <div className="col-3 actions_menu_icon">
                      <img className="" src={require('./images/message.jpg')} alt="Video"/>
                      <span>Video</span>
                    </div>
                    <div className="col-3 actions_menu_icon">
                      <img className="" src={require('./images/message.jpg')} alt="Transfer files"/>
                      <span>Transfer</span>
                    </div>
                  </div>
                </div>
                <div className="col-3 actions_menu_icon">
                  <img className="" src={require('./images/reliability.jpg')} alt="Blishers"/>
                  Reliability
                </div>
            </div>
          </div>
      </div>
      <div className="row transaction_menu">
        <ul>
          {this.transaction_menu(this.props.active)}
        </ul>
      </div>
    </div>

    );
  }
}

export default TransactionsTopBar;
