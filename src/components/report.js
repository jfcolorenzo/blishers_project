import React, { Component } from 'react';

import TransactionsTopBar from './transactions_top_bar'
require('./styles/report.css');

class Report extends Component {

  render(){
    return(

      <div className="container container_report">

        <TransactionsTopBar active="2"/>

        <div className="row grid_report">

          <div className="col-1">
          </div>
          <div className="col-10">
                      <div className="container grid_with_border">

                        <div className="row">
                          <div className="col-6">
                            <span><img className="report_logo" src={require('./images/logo.jpg')} alt="Blishers"/></span>
                            <p className="from_to"><span className="bold">From:</span> promopress<br />
                            <span className="bold">To:</span> Linea Editorial</p>
                            <span><img src={require('./images/cover.jpg')} alt="Cover"/></span>
                          </div>
                        <div className="col-6">
                          <div className="report_detail_list">
                            <ul>
                              <li><span className="bold">Order Number:</span> 0001</li>
                              <li><span className="bold">Date:</span> 12.12.2017</li>
                              <li><span className="bold">Total Amount:</span> 16800 usd</li>
                              <li><span className="bold">Payment Conditions:</span> 120 Days</li>
                              <li><span className="bold">Payment Method:</span> Transfer</li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="row">

                          <div className="col-6">
                            <div className="row report_grid_row">
                              <div className="col-3 grid_with_border cell_title">
                                Title
                              </div>
                              <div className="col-9 grid_with_border">
                                <span className="report_highlight">ECOLOGICAL GRAPHIC DESIGN</span>
                              </div>
                            </div>
                            <div className="row report_grid_row">
                              <div className="col-3 grid_with_border cell_title">
                                Size cm
                              </div>
                              <div className="col-9 grid_with_border">
                                <span className="report_highlight">24 x 26</span>
                              </div>
                            </div>
                            <div className="row report_grid_row">
                              <div className="col-3 grid_with_border cell_title">
                                Pages
                              </div>
                              <div className="col-9 grid_with_border">
                                <span className="report_highlight">320</span>
                              </div>
                            </div>
                            <div className="row report_grid_row">
                              <div className="col-3 grid_with_border cell_title">
                                Retail Price
                              </div>
                              <div className="col-9 grid_with_border">
                                <span className="report_highlight">39,95</span>
                              </div>
                            </div>
                            <div className="row report_grid_row">
                              <div className="col-3 grid_with_border cell_title">
                                Original Language
                              </div>
                              <div className="col-9 grid_with_border">
                                <span className="report_highlight">Spanish</span> <span className="report_highlight">English</span>
                              </div>
                            </div>
                          </div>

                          <div className="col-6">
                            <div className="row report_grid_row">
                              <div className="col-3 grid_with_border cell_title">
                                Countries
                              </div>
                              <div className="col-9 grid_with_border">
                                <span className="report_highlight">USA</span> <span className="report_highlight">CANADA</span> <span className="report_highlight">UK</span>
                              </div>
                            </div>
                            <div className="row report_grid_row">
                              <div className="col-3 grid_with_border cell_title">
                                Copies
                              </div>
                              <div className="col-9 grid_with_border">
                                <span className="report_highlight">8000</span>
                              </div>
                            </div>
                            <div className="row report_grid_row">
                              <div className="col-3 grid_with_border cell_title">
                                Price per Copy
                              </div>
                              <div className="col-9 grid_with_border">
                                <span className="report_highlight">2,10</span>
                              </div>
                            </div>
                            <div className="row report_grid_row">
                              <div className="col-3 grid_with_border cell_title">
                                Fees
                              </div>
                              <div className="col-9 grid_with_border">
                                <span className="report_highlight"></span>
                              </div>
                            </div>
                            <div className="row report_grid_row">
                              <div className="col-3 grid_with_border cell_title ">
                                Total $ Amount
                              </div>
                              <div className="col-9 grid_with_border">
                                <span className="report_highlight">16800</span>
                              </div>
                            </div>
                          </div>

                        </div>

                      </div>
                      <div className="container report_bottom">
                        <div className="row">
                          <div className="col">
                          </div>
                          <div className="col  center_vertically center_horizontally">
                            <div className="accept_order_button">Accept Order</div>
                          </div>
                          <div className="col tools_bottom">
                            <div><img src={require('./images/print.jpg')} alt="Print"/>PRINT</div><div><img src={require('./images/download.jpg')} alt="Download"/>DOWNLOAD</div>
                          </div>
                        </div>
                      </div>
          </div>
          <div className="col-1">
          </div>

        </div>
      </div>
    );
  }
}

export default Report;
