import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import promise from 'redux-promise';


import Home from './components/home';
import MyInfo from './components/my_info';
import Wannado from './components/wannado';
import Report from './components/report';
require('./style.css');



ReactDOM.render((

    <BrowserRouter>
      <div>
        <Switch>
          <Route path="/my_info" component={MyInfo} />
          <Route path="/wannado" component={Wannado} />
          <Route path="/report" component={Report} />
          <Route path="/" component={Home} />
        </Switch>
      </div>
    </BrowserRouter>

  ), document.getElementById('root'));
